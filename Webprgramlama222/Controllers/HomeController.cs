﻿using odev.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odev.Data.Domain;
using mobcommerce.Services.Categories;
using mobcommerce.Services.Products;

namespace Webprgramlama222.Controllers
{
    public class HomeController : Controller
    {
        private GenericUnitofWork unitofwork = null;
        public HomeController()
        {
            unitofwork = new GenericUnitofWork();
        }

        private List<Product> model;
        
        // GET: Home
        public ActionResult Index()
        {
            var model = new List<Product>();



            //ProductService _productService = new ProductService();
            //_productService.Insert(new Product
            //{

            //});
            //_productService.SaveChanges();
            
            model=  unitofwork.Repository<Product>().GetAll().ToList();
            return View(model);
        }
    }
}