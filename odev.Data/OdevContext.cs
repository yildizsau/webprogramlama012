﻿using odev.Data.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odev.Data
{
   public class CContex:DbContext
    {
        public CContex():base("odevcontext")
        {
            Database.SetInitializer<CContex>(new DropCreateDatabaseIfModelChanges<CContex>());
        }

        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().ToTable("Product").HasKey<int>(p => p.Id);
            modelBuilder.Entity<Category>()
                .ToTable("Category")
                .HasKey<int>(p => p.Id)
                //.Property(a => a.Name).IsRequired().HasMaxLength(60)
                .Property(p => p.ShortDescription).HasMaxLength(250);
            modelBuilder.Entity<ProductCategoryMapping>()
          .ToTable("ProductCategoryMapping").ToTable("ProductCategoryMapping")
          .Property(p=>p.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None);

            modelBuilder.Entity<Category>().Property(p => p.LongDescription).HasMaxLength(600);

            base.OnModelCreating(modelBuilder);
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
