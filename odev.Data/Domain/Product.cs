﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odev.Data.Domain
{
    //[Table("Product")]
    public class Product:BaseEntity, AuditEntity1
    {
        private IList<ProductCategoryMapping> _productCategories;

        public Product()
        {
            _productCategories=new List<ProductCategoryMapping> ();
        }

        public string Name { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }

        public decimal OldPrice { get; set; }
        public decimal Price { get; set; }
        public decimal CostPrice { get; set; }

        //Denetleme implamenti
        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedUserId { get; set; }
        public DateTime UpdatedDate { get; set; }


        public IList<ProductCategoryMapping> ProductCategories { get; set; }


    }
}
