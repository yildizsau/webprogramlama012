﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odev.Data.Domain
{
    [Table("Product_Category_Mapping")]
    public class ProductCategoryMapping : BaseEntity
    {
        public int ProductId { get; set; }
        public int CategoryId { get; set; }


        public Product Product { get; set; }
        public Category Category { get; set; }


    }
}
