﻿using System;

using System.ComponentModel.DataAnnotations.Schema;

namespace odev.Data.Domain
{
    //[Table("Category", Schema ="Production")]
   public  class Category:BaseEntity, AuditEntity1
    {
        public string Name { get; set; }
        public int ParentId { get; set; }
        public int PictureId { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }


        public int CreatedUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UpdatedUserId { get; set; }
        public DateTime UpdatedDate { get; set; }

    }
}
