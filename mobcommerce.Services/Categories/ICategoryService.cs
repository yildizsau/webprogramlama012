﻿using odev.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mobcommerce.Services.Categories
{
    interface ICategoryService
    {
        IList<Category> GetSubCategoriesById(int categoryId);


    }
}
