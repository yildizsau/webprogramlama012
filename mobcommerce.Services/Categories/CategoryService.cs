﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using odev.Data.Domain;
using odev.Data;

namespace mobcommerce.Services.Categories
{
    public partial class CategoryService : GenericRepository<CContex,Category>,ICategoryService
    {
        

    public IList<Category> GetSubCategoriesById(int categoryId)
        {
          return  this.Where(p => p.ParentId == categoryId).ToList();
        }

       
    }
}
